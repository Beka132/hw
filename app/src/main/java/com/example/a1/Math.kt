package com.example.a1

open class Math (){

    fun usj(firstVar:Int,secondVar:Int):Int { //usj ანუ ურთიერთ საერთო ჯერადი
        var a = firstVar
        var b = secondVar

        var c=1     // ცვლადი c შემოტანილია control flow statement while-ის ტერმინაციისათვის
        var usj=if(firstVar>secondVar)firstVar else secondVar
        while(c>0){
            if(usj%firstVar==0&&usj%secondVar==0){
                c=0     // c=0 ანუ პირობა c>0 აღარაა დაკმაყოფილებული რაც ნიშნავს უკანასკნელ ციკლის
            } else if(usj>2147000000){
                println("${a}s da ${b}s urtiert saerto jeradi ar aqvt")
                return 0
            }
            else usj++
        }
        return usj
    }

    fun usg(firstVar:Int,secondVar:Int):Int{    //usg ანუ ურთიერთ საერთო გამყოფი
        var a=firstVar
        var b=secondVar
        while (b>0){
            if (firstVar%a==0&&secondVar%a==0)b=0
            else a--
        }
        return a
    }

    fun string(string:String):String{
        return string // სიმბოლო $-ს გამოყენება არ არის საჭირო თუმცა შეგვიძლია გამოვიყენოთ შემდეგნაირათ println($string)
    }

    var sum=0 // ლუწი რიცხვების ჯამი

    fun recursiveFun(number:Int):Int{

        if (number>0){

            if(number%2==0) {
                sum += number
                var hundred = number - 1
                recursiveFun(hundred)
            } else {
                var hundred=number-1
                recursiveFun(hundred)
            }
        }

        return sum
    }

    fun invertNumber(number:Int):Int{ 
        var varNumber=number
        var inverted=0    // შებრუნებული რიცხვი
        var decimal=1    // ეს ცვლადი აღნიშავს თუ რომელ ათეულში/ათასეულში თუ ერთეულში იყო რიცხვი
        while (varNumber>0){
            varNumber/=10
            decimal*=10
        }
        varNumber=number
        while(varNumber>0){
            inverted+=(varNumber%10)*decimal
            varNumber/=10
            decimal/=10
        }
        while (inverted%10==0){    //ზედმეტი ნულების მოსაშორებლადაა ეს ციკლი
            inverted/=10
        }

        return inverted

    }

    fun isPalindrome(word:String):Boolean{
        val word = word.toLowerCase().replace(" ","")    // იმისათვის რომ ტოლობა იყოს მხოლოდ ასოების, ყველა ასო უნდა იყოს პატარა და ასევე წინადადებებში სფეისები უნდა წაიშალოს
        val invertedWord=word.reversed()
        return word==invertedWord    //თუ სიტყვა უდრის შებრუნებულ სიტყვას მაშინ ეს ხაზი მოგვცემს მნიშვნელობა "true" ხოლო წინააღმდეგ შემთხვევაში პასუხი იქნება "false"

    }

}

fun main() {
    val test=Math()

    println(test.usg(23124,22))
    println(test.usj(23124,22))
    println(test.string("Hello World"))
    println(test.recursiveFun(99)) //მოცემული ფუნქციის პარამეტრში ჩავწერთ რიცხვს თუ რომლის ჩათვლით გვინდა ლუწი რიცხვების ჯამი და მივიღებთ პასუხს
    println(test.invertNumber(10220))
    println(test.isPalindrome("Murder for a jar of red rum"))
}